require 'test_helper'

class EmployeesControllerTest < ActionController::TestCase
  setup do
    @employee = employees(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:employees)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create employee" do
    assert_difference('Employee.count') do
      post :create, employee: { city: @employee.city, employee_address: @employee.employee_address, employee_email: @employee.employee_email, employee_name: @employee.employee_name, employee_phone: @employee.employee_phone, social_security: @employee.social_security, state: @employee.state, zip_code: @employee.zip_code }
    end

    assert_redirected_to employee_path(assigns(:employee))
  end

  test "should show employee" do
    get :show, id: @employee
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @employee
    assert_response :success
  end

  test "should update employee" do
    patch :update, id: @employee, employee: { city: @employee.city, employee_address: @employee.employee_address, employee_email: @employee.employee_email, employee_name: @employee.employee_name, employee_phone: @employee.employee_phone, social_security: @employee.social_security, state: @employee.state, zip_code: @employee.zip_code }
    assert_redirected_to employee_path(assigns(:employee))
  end

  test "should destroy employee" do
    assert_difference('Employee.count', -1) do
      delete :destroy, id: @employee
    end

    assert_redirected_to employees_path
  end
end
