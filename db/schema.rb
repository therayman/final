# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141202025225) do

  create_table "appointments", force: true do |t|
    t.integer  "physician_id"
    t.integer  "patient_id"
    t.datetime "date"
    t.text     "reason"
    t.text     "note"
    t.integer  "diagnostic_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "status_id"
  end

  create_table "diagnostic_codes", force: true do |t|
    t.string   "code"
    t.string   "description"
    t.float    "fee"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "diagnostics", force: true do |t|
    t.float    "diagnosticcode"
    t.string   "description"
    t.float    "fee"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "employees", force: true do |t|
    t.string   "employee_name"
    t.string   "employee_address"
    t.string   "city"
    t.string   "state"
    t.string   "zip_code"
    t.string   "employee_email"
    t.string   "employee_phone"
    t.string   "social_security"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "insurances", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invoices", force: true do |t|
    t.integer  "employee_id"
    t.integer  "appointment_id"
    t.float    "total"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "patients", force: true do |t|
    t.string   "patient_name"
    t.string   "patient_address"
    t.string   "city"
    t.string   "state"
    t.string   "zip_code"
    t.string   "patient_email"
    t.string   "patient_phone"
    t.integer  "insurance_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "physicians", force: true do |t|
    t.string   "physician_name"
    t.string   "physician_address"
    t.string   "city"
    t.string   "state"
    t.string   "zip_code"
    t.string   "physician_email"
    t.string   "physician_phone"
    t.string   "social_security"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "statuses", force: true do |t|
    t.string   "status_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
