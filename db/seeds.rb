# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'csv'

Diagnostic.delete_all
open("/vagrant/projects/Final/Prices.csv") do |p|
  p.read.each_line do |p|
    diagnosticcode, description, fee = p.chomp.split(",")
    Diagnostic.create!(diagnosticcode: diagnosticcode, description: description, fee: fee)
  end
end