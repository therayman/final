class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.integer :employee_id
      t.integer :appointment_id
      t.float :total

      t.timestamps
    end
  end
end
