class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :patient_name
      t.string :patient_address
      t.string :city
      t.string :state
      t.string :zip_code
      t.string :patient_email
      t.string :patient_phone
      t.integer :insurance_id

      t.timestamps
    end
  end
end
