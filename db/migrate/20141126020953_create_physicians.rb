class CreatePhysicians < ActiveRecord::Migration
  def change
    create_table :physicians do |t|
      t.string :physician_name
      t.string :physician_address
      t.string :city
      t.string :state
      t.string :zip_code
      t.string :physician_email
      t.string :physician_phone
      t.string :social_security

      t.timestamps
    end
  end
end
