class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :employee_name
      t.string :employee_address
      t.string :city
      t.string :state
      t.string :zip_code
      t.string :employee_email
      t.string :employee_phone
      t.string :social_security

      t.timestamps
    end
  end
end
