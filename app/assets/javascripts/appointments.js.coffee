
jQuery ->
$("#datetimepicker_mask").datetimepicker(
  beforeShowDay: $.datepicker.noWeekends, minDate:0,
  allowTimes: ['8:00', '8:30', '9:00', '9:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '16:30', '16:00', '16:30' ],
  mask: true

);