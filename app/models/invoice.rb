class Invoice < ActiveRecord::Base
  has_one :appointment
  belongs_to :employee
  belongs_to :appointment
end
