class Diagnostic < ActiveRecord::Base
  has_one :appointment

  validates_presence_of :fee
  validates(:fee, :numericality => {:greater_than_or_equal_to => 0, :message => 'Positive amount only'})
end
