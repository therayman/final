class Appointment < ActiveRecord::Base
  belongs_to :patient
  belongs_to :diagnostic
  belongs_to :physician
  belongs_to :status
  belongs_to :invoice

  validate :date, uniqueness: true
end
