class Patient < ActiveRecord::Base
 belongs_to :insurance
  has_many :appointments
  has_many :physicians, through: :appointments
end
