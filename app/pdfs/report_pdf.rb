 class ReportPdf < Prawn::Document
   def initialize (appointment)
     super(top_margin: 80)
       @appointment = appointment

     appointment_number

     appoint_information




   end
   def appointment_number
     text "Appointment \##{@appointment.id}", size:30, style: :bold

   end

    def appoint_information
     move_down 30
     table appointment_patient do
end
    end

   def appointment_patient
     [["Patient", "Physician", "Date", "Reason", "Note", "Diagnostic"]] +
     @appointment.notice.map do |item|
       [item.physician.physician_name, item.patient.patient_name, item.date, item.reason, item.note, item.diagnostic.description]
     end
     end

 end