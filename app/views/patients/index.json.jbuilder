json.array!(@patients) do |patient|
  json.extract! patient, :id, :patient_name, :patient_address, :city, :state, :zip_code, :patient_email, :patient_phone, :insurance_id
  json.url patient_url(patient, format: :json)
end
