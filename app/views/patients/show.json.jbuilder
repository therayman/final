json.extract! @patient, :id, :patient_name, :patient_address, :city, :state, :zip_code, :patient_email, :patient_phone, :insurance_id, :created_at, :updated_at
