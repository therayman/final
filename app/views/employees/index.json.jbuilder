json.array!(@employees) do |employee|
  json.extract! employee, :id, :employee_name, :employee_address, :city, :state, :zip_code, :employee_email, :employee_phone, :social_security
  json.url employee_url(employee, format: :json)
end
