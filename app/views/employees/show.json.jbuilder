json.extract! @employee, :id, :employee_name, :employee_address, :city, :state, :zip_code, :employee_email, :employee_phone, :social_security, :created_at, :updated_at
