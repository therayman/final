json.array!(@invoices) do |invoice|
  json.extract! invoice, :id, :employee_id, :appointment_id, :total
  json.url invoice_url(invoice, format: :json)
end
