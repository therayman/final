json.array!(@physicians) do |physician|
  json.extract! physician, :id, :physician_name, :physician_address, :city, :state, :zip_code, :physician_email, :physician_phone, :social_security
  json.url physician_url(physician, format: :json)
end
