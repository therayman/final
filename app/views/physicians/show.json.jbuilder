json.extract! @physician, :id, :physician_name, :physician_address, :city, :state, :zip_code, :physician_email, :physician_phone, :social_security, :created_at, :updated_at
